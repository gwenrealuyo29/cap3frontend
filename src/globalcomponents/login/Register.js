import React from "react";

export class Register extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="base-container" ref={this.props.containerRef}>
        <div className="header">Register</div>
        <div className="content">
          <div className="form">
            <div className="form-group">
              <label htmlFor="username">Username</label>
              <input
                type="text"
                name="username"
                placeholder="Enter username"
                onChange={this.props.handleUsernameChange}
              />
            </div>
            <div className="form-group">
              <label htmlFor="email">Email</label>
              <input
                type="email"
                name="email"
                placeholder="Enter email"
                onChange={this.props.handleEmailChange}
              />
            </div>
            <div className="form-group">
              <label htmlFor="password">Password</label>
              <input
                type="password"
                name="password"
                placeholder="Enter password"
                onChange={this.props.handlePasswordChange}
              />
            </div>
          </div>
        </div>
        <div className="footer">
          <button
            disabled={
              this.props.email == "" ||
              this.props.username == "" ||
              this.props.password == ""
                ? true
                : false
            }
            type="submit"
            className="btn"
            onClick={this.props.handleRegister}
          >
            Register
          </button>
        </div>
      </div>
    );
  }
}
