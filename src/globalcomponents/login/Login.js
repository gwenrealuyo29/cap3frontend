import React from "react";

export class Login extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="base-container" ref={this.props.containerRef}>
        <img src="icon.PNG" />
        <div className="header">Login</div>
        <div className="content">
          <div className="form">
            <div className="form-group">
              <label htmlFor="email">Email</label>
              <input
                type="email"
                name="email"
                placeholder="Enter email"
                onChange={this.props.handleEmailChange}
              />
            </div>
            <div className="form-group">
              <label htmlFor="password">Password</label>
              <input
                type="password"
                name="password"
                placeholder="Enter password"
                onChange={this.props.handlePasswordChange}
              />
            </div>
          </div>
        </div>
        <div className="footer">
          <button
            type="submit"
            disabled={
              this.props.email == "" || this.props.password == "" ? true : false
            }
            className="btn"
            onClick={this.props.handleLogin}
          >
            Login
          </button>
        </div>
      </div>
    );
  }
}
