import React from "react";
import { HashRouter, Route, Switch } from "react-router-dom";
import { css } from "@emotion/core";
import { PropagateLoader } from "react-spinners";

const loading = () => {
  return (
    // <div className="bg-dark vh-100 d-flex justify-content-center align-items-center">
    //   <h1>Loading..</h1>
    // </div>

    <div className="bg-white vh-100 d-flex justify-content-center align-items-center">
      <PropagateLoader
        css={override}
        sizeUnit={"px"}
        size={15}
        color={"#FFAF06"}
        loading={true}
      />
    </div>
  );
};

const override = css`
  display: block;
  margin: 0 auto;
  border-color: white;
`;

const Areas = React.lazy(() => import("./views/Area/Areas"));
const Inquiries = React.lazy(() => import("./views/Inquiry/Inquiries"));
const Signin = React.lazy(() => import("./views/Pages/Signin"));
const Landing = React.lazy(() => import("./views/Pages/Landing"));
const Home = React.lazy(() => import("./views/Pages/Home"));
const MyBookings = React.lazy(() => import("./views/Pages/MyBookings"));
const Payment = React.lazy(() => import("./views/Pages/Payment"));

const App = () => {
  return (
    <HashRouter>
      <React.Suspense fallback={loading()}>
        <Switch>
          <Route
            path="/areas"
            exact
            name="Areas"
            render={props => <Areas {...props} />}
          />
          <Route
            path="/inquiries"
            exact
            name="Inquiries"
            render={props => <Inquiries {...props} />}
          />
          <Route
            path="/login"
            exact
            name="Login"
            render={props => <Signin {...props} />}
          />
          <Route
            path="/"
            exact
            name="Landing"
            render={props => <Landing {...props} />}
          />
          <Route
            path="/home"
            exact
            name="Home"
            render={props => <Home {...props} />}
          />
          <Route
            path="/mybookings"
            exact
            name="My Bookings"
            render={props => <MyBookings {...props} />}
          />
          <Route
            path="/payment"
            exact
            name="Payment"
            render={props => <Payment {...props} />}
          />
          />
        </Switch>
      </React.Suspense>
    </HashRouter>
  );
};

export default App;
