import React, { useState } from "react";
import {
  Button,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";
import { Logs } from "../components";
import { FormInput } from "../../../globalcomponents";

const InquiriesRow = props => {
  const [dropdownOpen, setDropdownOpen] = useState(false);

  const toggle = () => {
    setDropdownOpen(!dropdownOpen);
  };

  const [showLogs, setShowLogs] = useState(false);

  const handleShowLogs = () => {
    setShowLogs(!showLogs);
  };

  const inquiry = props.inquiry;
  return (
    <React.Fragment>
      <tr>
        <td>{inquiry.code}</td>
        <td>{inquiry.inquiryDate}</td>
        <td>{inquiry.areaName}</td>
        <td onClick={() => props.handleShowPopulation(inquiry._id)}>
          {props.showPopulation && props.editId === inquiry._id ? (
            <FormInput
              defaultValue={inquiry.population}
              type={"number"}
              autoFocus={true}
              onBlur={e => props.handleEditPopulationInput(e, inquiry)}
            />
          ) : (
            inquiry.population
          )}
        </td>
        <td>{inquiry.inquirer}</td>
        <td onClick={() => props.handleShowStatus(inquiry._id)}>
          {props.showStatus && props.editId === inquiry._id ? (
            <Dropdown isOpen={dropdownOpen} toggle={toggle}>
              <DropdownToggle caret>Update Status</DropdownToggle>
              <DropdownMenu>
                <DropdownItem
                  onClick={() =>
                    props.handleStatusInput("Processing", inquiry.logs)
                  }
                >
                  Processing
                </DropdownItem>
                <DropdownItem
                  onClick={() => props.handleStatusInput("Paid", inquiry.logs)}
                >
                  Paid
                </DropdownItem>
                <DropdownItem
                  onClick={() =>
                    props.handleStatusInput("Approved", inquiry.logs)
                  }
                >
                  Approved
                </DropdownItem>
                <DropdownItem
                  onClick={() =>
                    props.handleStatusInput("Rejected", inquiry.logs)
                  }
                >
                  Rejected
                </DropdownItem>
              </DropdownMenu>
            </Dropdown>
          ) : (
            inquiry.status
          )}
        </td>
        <td>{inquiry.approver}</td>
        <td>
          <Button
            outline
            color="danger"
            onClick={() => props.handleDeleteInquiry(inquiry._id)}
          >
            <i class="fas fa-trash-alt"></i>
          </Button>
          {"  "}
          <Button outline color="secondary" onClick={handleShowLogs}>
            View Logs
          </Button>
        </td>
      </tr>
      <Logs
        handleShowLogs={handleShowLogs}
        showLogs={showLogs}
        inquiry={inquiry}
      />
    </React.Fragment>
  );
};

export default InquiriesRow;
