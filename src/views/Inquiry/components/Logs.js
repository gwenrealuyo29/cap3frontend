import React from "react";
import {
  Modal,
  ModalHeader,
  ModalBody,
  ListGroup,
  ListGroupItem
} from "reactstrap";

const Logs = props => {
  return (
    <Modal isOpen={props.showLogs} toggle={props.handleShowLogs}>
      <ModalHeader toggle={props.handleShowLogs}>Logs</ModalHeader>
      <ModalBody>
        <h1 className="text-center">Logs for Request #{props.inquiry.code}</h1>
        <h5>Area Inquired: {props.inquiry.areaName}</h5>
        <h5>Inquired By: {props.inquiry.inquirer}</h5>
        <h3 className="text-center py-3">History: </h3>
        <ListGroup>
          {props.inquiry.logs.map(log => (
            <ListGroupItem key={log._id}>
              {log.message}
              <br />
              {log.updateDate}
            </ListGroupItem>
          ))}
        </ListGroup>
      </ModalBody>
    </Modal>
  );
};

export default Logs;
