import React, { useState, useEffect } from "react";
import {
  Modal,
  ModalBody,
  ModalHeader,
  Button,
  FormGroup,
  Label,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";
import { FormInput } from "../../../globalcomponents";
import axios from "axios";

const InquiryForm = props => {
  const [dropdownOpen, setDropdownOpen] = useState(false);

  const toggle = () => {
    setDropdownOpen(!dropdownOpen);
  };

  const [areas, setAreas] = useState([]);

  useEffect(() => {
    axios.get("https://nittygrittycoworking.herokuapp.com/admin/showareas").then(res => {
      setAreas(res.data);
    });
  }, []);
  return (
    <React.Fragment>
      <Modal isOpen={props.showForm} toggle={props.handleShowForm}>
        <ModalHeader toggle={props.handleShowForm}>Add Inquiry</ModalHeader>
        <ModalBody>
          <FormGroup>
            <Label>Area Name</Label>
            <Dropdown isOpen={dropdownOpen} toggle={toggle}>
              <DropdownToggle caret>
                {props.areaName === "" ? "Choose Area" : props.areaName}
              </DropdownToggle>
              <DropdownMenu>
                {areas.map(area => (
                  <DropdownItem
                    key={area._id}
                    onClick={() => props.handleAreaInput(area)}
                  >
                    {area.name}
                  </DropdownItem>
                ))}
              </DropdownMenu>
            </Dropdown>
          </FormGroup>
          <FormInput
            name={"population"}
            label={"Number of People"}
            type={"number"}
            placeholder={"Enter number of people"}
            onChange={props.handlePopulationInput}
            defaultValue={1}
          />
          <Button
            color="warning"
            block
            outline
            onClick={props.handleSaveInquiry}
            disabled={
              props.areaName == "" || props.population <= 0 ? true : false
            }
          >
            Add Inquiry
          </Button>
        </ModalBody>
      </Modal>
    </React.Fragment>
  );
};

export default InquiryForm;
