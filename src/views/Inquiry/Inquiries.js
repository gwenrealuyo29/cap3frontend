import React, { useEffect, useState } from "react";
import { InquiriesRow, InquiryForm } from "./components";
import axios from "axios";
import Swal from "sweetalert2";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Button } from "reactstrap";
import { NavBar } from "../Layout";
import moment from "moment";
import CsvDownloader from "react-csv-downloader";

const Inquiries = () => {
  const baseurl = "https://nittygrittycoworking.herokuapp.com";

  const [inquiries, setInquiries] = useState([]);
  const [showForm, setShowForm] = useState(false);
  const [areaName, setAreaName] = useState("");
  const [areaId, setAreaId] = useState("");
  const [population, setPopulation] = useState(1);
  const [editId, setEditId] = useState("");
  const [showStatus, setShowStatus] = useState(false);
  const [showPopulation, setShowPopulation] = useState(false);
  const [user, setUser] = useState([]);

  const columns = [
    {
      id: "first",
      displayName: "First column"
    },
    {
      id: "second",
      displayName: "Second column"
    }
  ];

  const datas = [
    {
      first: "foo",
      second: "bar"
    },
    {
      first: "foobar",
      second: "foobar"
    }
  ];

  useEffect(() => {
    if (sessionStorage.token) {
      let user = JSON.parse(sessionStorage.user);
      setUser(user);
      if (user.isAdmin) {
        fetch(baseurl + "/admin/showinquiries")
          .then(res => res.json())
          .then(data => setInquiries(data));
      } else {
        window.location.replace("#/home");
      }
    } else {
      window.location.replace("#/login");
    }
  }, []);

  const handleShowForm = () => {
    setShowForm(!showForm);
  };

  const handleRefresh = () => {
    setShowForm(false);
    setAreaName("");
    setAreaId("");
    setPopulation(1);
    setEditId("");
  };

  const handleDeleteInquiry = inquiry => {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!"
    }).then(result => {
      if (result.value) {
        Swal.fire("Deleted!", "It has been deleted.", "success");

        axios.delete(baseurl + "/admin/deleteinquiry/" + inquiry).then(res => {
          let index = inquiries.findIndex(i => i._id === inquiry);
          let newInquiries = [...inquiries];
          newInquiries.splice(index, 1);
          setInquiries(newInquiries);
        });
      }
    });
  };

  const handleAreaInput = area => {
    setAreaName(area.name);
    setAreaId(area._id);
  };

  const handlePopulationInput = e => {
    setPopulation(e.target.value);
  };

  const handleSaveInquiry = () => {
    let code = moment(new Date()).format("x");

    // hardcoded
    let inquirer = user.email;
    let inquirerId = user.id;
    let approver = "Gwen";
    let approverId = "<3";

    axios
      .post(baseurl + "/admin/addinquiry", {
        code: code,
        population: population,
        areaName: areaName,
        areaId: areaId,
        inquirer: inquirer,
        inquirerId: inquirerId,
        approver: approver,
        approverId: approverId
      })
      .then(res => {
        let newInquiries = [...inquiries];
        newInquiries.push(res.data);
        setInquiries(newInquiries);
        handleRefresh();
      });
  };

  const handleShowStatus = editId => {
    setEditId(editId);
    setShowStatus(true);
    setShowPopulation(false);
  };

  const handleShowPopulation = editId => {
    setEditId(editId);
    setShowPopulation(true);
    setShowStatus(false);
  };

  const handleStatusInput = (status, logs) => {
    let log = {
      status: "Pending",
      updateDate: moment(new Date()).format("MM/DD/YYYY"),
      message: "Updated Status to " + status + " by " + user.username
    };

    axios
      .patch(baseurl + "/admin/updateinquirystatus/" + editId, {
        status,
        logs: log,
        approver: user.username,
        approverId: user._id
      })
      .then(res => {
        let index = inquiries.findIndex(i => i._id === editId);
        let newInquiry = [...inquiries];
        newInquiry.splice(index, 1, res.data);
        setInquiries(newInquiry);
        setEditId("");
      });
  };

  const handleEditPopulationInput = (e, inquiry) => {
    let oldPopulation = inquiry.population;
    let population = e.target.value;

    let log = {
      status: "Pending",
      updateDate: moment(new Date()).format("MM/DD/YYYY"),
      message: "Updated Population from " + oldPopulation + " to " + population
    };

    axios
      .patch(baseurl + "/admin/updatepopulation/" + editId, {
        population,
        logs: log
      })
      .then(res => {
        let index = inquiries.findIndex(inquiry => inquiry._id == editId);
        let newInquiry = [...inquiries];
        newInquiry.splice(index, 1, res.data);
        setInquiries(newInquiry);
        handleRefresh();
      });
  };

  return (
    <React.Fragment>
      <NavBar />
      <ToastContainer />
      <div className="col-lg-10 offset-lg-1 pt-5">
        <h1 className="text-center pt-5 px-5 mulifont">Workspace Inquiries</h1>

        <div className="d-flex justify-content-end robotofont">
          <Button outline color="warning" onClick={handleShowForm}>
            <i class="fas fa-plus"></i> Inquiry
          </Button>
          <InquiryForm
            showForm={showForm}
            handleShowForm={handleShowForm}
            handleAreaInput={handleAreaInput}
            areaName={areaName}
            handlePopulationInput={handlePopulationInput}
            population={population}
            handleSaveInquiry={handleSaveInquiry}
          />
        </div>

        <table className="table table-striped table-hover border mt-2 robotofont">
          <thead>
            <tr>
              <th>Inquiry Code</th>
              <th>Date Inquired</th>
              <th>Area Inquired</th>
              <th>Number of People</th>
              <th>Inquired by</th>
              <th>Status</th>
              <th>Approved / Rejected By</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {inquiries.map(inquiry => (
              <InquiriesRow
                key={inquiry._id}
                inquiry={inquiry}
                inquiries={inquiries}
                handleDeleteInquiry={handleDeleteInquiry}
                handleShowStatus={handleShowStatus}
                showStatus={showStatus}
                editId={editId}
                handleStatusInput={handleStatusInput}
                handleShowPopulation={handleShowPopulation}
                showPopulation={showPopulation}
                handleEditPopulationInput={handleEditPopulationInput}
              />
            ))}
          </tbody>
        </table>
        <div className="d-flex justify-content-center align-items-center">
          <CsvDownloader filename="inquiries" datas={inquiries} target="_blank">
            <Button outline color="info">
              Download as CSV
            </Button>
          </CsvDownloader>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Inquiries;
