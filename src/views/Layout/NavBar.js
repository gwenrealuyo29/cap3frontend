import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { Badge } from "reactstrap";

const NavBar = () => {
  const [user, setUser] = useState([]);
  const baseurl = "https://nittygrittycoworking.herokuapp.com";
  const [myInquiries, setMyInquiries] = useState(0);
  useEffect(() => {
    if (sessionStorage.token) {
      let user = JSON.parse(sessionStorage.user);
      // alert(user);
      setUser(user);
      console.log(user);
      fetch(baseurl + "/admin/showinquiriesbyuser/" + user.id)
        .then(res => res.json())
        .then(data => {
          let bookings = data.length;
          setMyInquiries(bookings);
        });
    }
  }, []);

  const handleLogout = () => {
    sessionStorage.clear();
    window.location.replace("#/");
  };

  return (
    <React.Fragment>
      <div className="mb-5">
        <nav className="navbar navbar-expand-lg fixed-top navbar-light bg-white robotofont py-4">
          <div className="collapse navbar-collapse">
            <ul className="navbar-nav mr-auto">
              <div className="navbar-nav pr-5">
                <img src="icon.PNG" alt="logo" height="50" />
                <img src="brand.PNG" alt="Nitty-Gritty" height="50" />
              </div>
              <li className="nav-item">
                <Link to="/home" className="nav-link">
                  Home
                </Link>
              </li>

              {sessionStorage.token != undefined && user.isAdmin ? (
                <React.Fragment>
                  <li className="nav-item">
                    <Link to="/areas" className="nav-link">
                      Areas
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/inquiries" className="nav-link">
                      Inquiries
                    </Link>
                  </li>
                </React.Fragment>
              ) : (
                <li className="nav-item">
                  <Link to="/mybookings" className="nav-link">
                    My Bookings{" "}
                    <Badge color="dark">
                      {sessionStorage.token ? myInquiries : ""}
                    </Badge>
                  </Link>
                </li>
              )}
            </ul>
            {sessionStorage.token != undefined ? (
              <ul className="navbar-nav ml-auto pb-2">
                <li className="nav-item">
                  <Link to="/" onClick={handleLogout} className="nav-link">
                    Logout
                  </Link>
                </li>
              </ul>
            ) : (
              ""
            )}
          </div>
        </nav>
      </div>
    </React.Fragment>
  );
};

export default NavBar;
