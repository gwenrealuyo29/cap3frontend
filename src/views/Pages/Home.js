import React, { useEffect, useState } from "react";
import { NavBar } from "../Layout";
import AreaList from "./AreaList";
import { Container, Row } from "reactstrap";

const Home = () => {
  const [user, setUser] = useState([]);
  const baseurl = "https://nittygrittycoworking.herokuapp.com";
  const [areas, setAreas] = useState([]);

  useEffect(() => {
    if (sessionStorage.token) {
      let user = JSON.parse(sessionStorage.user);
      // alert(user);
      console.log(user);
      setUser(user);
      fetch(baseurl + "/admin/showareas")
        .then(res => res.json())
        .then(data => setAreas(data));
    } else {
      window.location.replace("#/login");
    }
  }, []);

  return (
    <React.Fragment>
      <NavBar />
      <div className="pt-5">
        <Container>
          <h1 className="text-center pt-5 px-5 mulifont">
            Workspace Areas to Book
          </h1>
          <Row>
            {areas.map(area => (
              <AreaList key={area._id} area={area} />
            ))}
          </Row>
        </Container>
      </div>
    </React.Fragment>
  );
};

export default Home;
