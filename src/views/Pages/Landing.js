import React from 'react';
import "../../App.scss";

const Landing = () => {
	return(
		<React.Fragment>
			<div className="Landing">
				<h1 className="chonburifont" style={{color: "black", fontSize: "100px"}}>Nitty-Gritty</h1>
				<h3 className="robotofont" style={{color: "black", fontSize: "50px"}}>coworking spaces</h3>
			</div>

		</React.Fragment>
	)
}

export default Landing;