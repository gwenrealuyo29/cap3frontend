import React, { useState } from "react";
import { Modal, ModalHeader, ModalBody, Button } from "reactstrap";
import DayPicker from "react-day-picker";
import "react-day-picker/lib/style.css";
import moment from "moment";
import TimePicker from "rc-time-picker";
import "rc-time-picker/assets/index.css";
import "react-times/css/classic/default.css";
import { FormInput } from "../../globalcomponents";

const BookForm = props => {
  const format = "h:mm A";
  // let disabledDays;

  let closingDays = {
    daysOfWeek: [0, 7]
  };

  // const todisabledDays = this.props.bookedDates

    

  //   // to disable previous dates
  //   const today = [new Date()];
  //   let b = today.map(dd=>{
  //     return {before : new Date(moment(dd).format("MM/DD/YY")), after: new Date(moment(dd)).add(60, "days").format("MM/DD/YYYY") }
  //   })
  //   allDisabledDays.unshift(b[0])




  let past = {
    before: new Date()
  }
  // console.log(past, disabledDays)

// ,
//     after: moment(new Date()).add(60, "days").format("MM/DD/YYYY"),

  // disabledDays = disabledDays.concat(past)
  // disabledDays = {...closingDays, ...past}
  // console.log(disabledDays)

  // const closingHours = () => {
  //   if (props.pastHours == undefined) {
  //   }
  //   return [0, 1, 2, 3, 4, 5];
  // };

  // const pastHours = () => {
  //   let hours = props.pastHours;
  //   return hours;
  // };

  const closingMins = h => {
    switch (h) {
      case 23:
        return [30];
      default:
        return [];
    }
  };

  return (
    <div>
      <Modal isOpen={props.showForm} toggle={props.handleShowForm}>
        <ModalHeader toggle={props.handleShowForm}>
          Booking Area Name:{" "}
          <span className="font-italic">{props.area.name}</span>
        </ModalHeader>
        <ModalBody>
          <p>
            Area Code: <span className="font-italic">{props.area.code}</span>{" "}
          </p>
          <p>{props.area.description}</p>
          <p>Category: {props.area.category}</p>
          <p>PHP {props.area.price}.00</p>
          <p>
            <strong>Select Day:</strong>
          </p>

          <DayPicker
            className="Selectable"
            onDayClick={props.handleDayClick}
            selectedDays={props.selectedDay}
            disabledDays={[past, closingDays]}
          />

          {props.selectedDay ? (
            <p>You've selected {props.selectedDay.toLocaleDateString()}</p>
          ) : (
            <p>Please select a day.</p>
          )}

          <p>
            <strong>From:</strong>
          </p>

          <TimePicker
            showSecond={false}
            style={{ width: 150 }}
            defaultValue={moment()
              .hour("h")
              .minute("mm")}
            minuteStep={30}
            format={format}
            disabledHours={() => [0, 1, 2, 3, 4, 5]}
            disabledMinutes={closingMins}
            hideDisabledOptions
            onChange={props.handleFirstHour}
            className="xxx"
          />

          <p>
            <strong>To:</strong>
          </p>

          <TimePicker
            showSecond={false}
            style={{ width: 150 }}
            minuteStep={30}
            format={format}
            disabledHours={() => props.pastHour}
            disabledMinutes={closingMins}
            hideDisabledOptions
            onChange={props.handleSecondHour}
            className="xxx"
            disabled={
              props.hourFrom == "" || props.hourFrom == undefined ? true : false
            }
          />

          <p></p>

          <FormInput
            name={"population"}
            label={"Number of People"}
            type={"number"}
            placeholder={"Enter number of people"}
            onChange={props.handlePopulationInput}
            defaultValue={
              props.area.category == "Partners in Crime"
                ? 2
                : props.area.category == "Groupie"
                ? 5
                : 1
            }
            readOnly={
              props.area.category == "Partners in Crime" ||
              props.area.category == "Soloist"
                ? true
                : false
            }
          />

          <Button
            color="success"
            outline
            onClick={props.handleSaveBooking}
            block
            disabled={
              props.selectedDay == undefined ||
              props.hourFrom == "" ||
              props.hourTo == "" ||
              props.population == 0
            }
          >
            Save Booking
          </Button>
        </ModalBody>
      </Modal>
    </div>
  );
};
export default BookForm;
