import React, { useEffect, useState } from "react";
import { NavBar } from "../Layout";
import InquiryList from "./InquiryList";
import { Container, Row } from "reactstrap";
import Swal from "sweetalert2";
import axios from "axios";

const MyBookings = () => {
  const [user, setUser] = useState([]);
  const baseurl = "https://nittygrittycoworking.herokuapp.com";
  const [inquiries, setInquiries] = useState([]);
  const [inquiry, setInquiry] = useState([]);
  const [areas, setAreas] = useState([]);

  useEffect(() => {
    if (sessionStorage.token) {
      let user = JSON.parse(sessionStorage.user);
      // alert(user);
      setUser(user);
      console.log(user);
      fetch(baseurl + "/admin/showinquiriesbyuser/" + user.id)
        .then(res => res.json())
        .then(data => {
          setInquiries(data);
        });
    } else {
      window.location.replace("#/login");
    }
  }, []);

  const handleDeleteBooking = inquiry => {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, cancel it!"
    }).then(result => {
      if (result.value) {
        Swal.fire("Cancelled!", "Your booking has been cancelled.", "success");

        axios.delete(baseurl + "/admin/deleteinquiry/" + inquiry).then(res => {
          let index = inquiries.findIndex(i => i._id === inquiry);
          let newInquiries = [...inquiries];
          newInquiries.splice(index, 1);
          setInquiries(newInquiries);
        });
      }
    });
  };

  return (
    <React.Fragment>
      <NavBar />

      <Container className="robotofont pt-5">
        <h1 className="text-center pt-5 px-5 mulifont">My Bookings</h1>
        <Row>
          {inquiries.map(inquiry => (
            <InquiryList
              key={inquiry._id}
              inquiry={inquiry}
              handleDeleteBooking={handleDeleteBooking}
            />
          ))}
        </Row>
      </Container>
    </React.Fragment>
  );
};

export default MyBookings;
