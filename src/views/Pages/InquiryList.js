import React, { useState, useEffect } from "react";
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button
} from "reactstrap";
import axios from "axios";
import moment from "moment";
import Payment from "./Payment";
import Swal from "sweetalert2";

const InquiryList = props => {
  const baseurl = "https://nittygrittycoworking.herokuapp.com";
  const [user, setUser] = useState([]);
  const inquiry = props.inquiry;
  const [showStripe, setShowStripe] = useState(false);
  const [inquiries, setInquiries] = useState([]);

  useEffect(() => {
    if (sessionStorage.token) {
      let user = JSON.parse(sessionStorage.user);
      setUser(user);
      fetch("https://nittygrittycoworking.herokuapp.com/admin/showinquiriesbyuser/" + user.id)
        .then(res => res.json())
        .then(data => setInquiries(data));
    } else {
      window.location.replace("#/login");
    }
  }, []);

  const handleShowStripe = () => {
    setShowStripe(!showStripe);
  };

  const handleCancelBooking = () => {
    setShowStripe(false);
  };

  return (
    <React.Fragment>
      <Payment
        showStripe={showStripe}
        handleShowStripe={handleShowStripe}
        inquiry={inquiry}
        handleCancelBooking={handleCancelBooking}
      />

      <div className="col-lg-4 my-5">
        <Card body className="text-center">
          <CardImg
            top
            width="100%"
            src={
              inquiry.areaName === "Friendssss"
                ? "https://images.pexels.com/photos/1170412/pexels-photo-1170412.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
                : inquiry.areaName === "Solo Place"
                ? "https://images.pexels.com/photos/159839/office-home-house-desk-159839.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
                : "https://images.pexels.com/photos/37347/office-sitting-room-executive-sitting.jpg?auto=compress&cs=tinysrgb&dpr=1&w=500"
            }
            alt="IMAGE COMING SOON!!!"
          />
          <CardBody>
            <CardTitle className="chonburifont">{inquiry.areaName}</CardTitle>

            <CardText className="robotofont">
              Code:
              <span className="font-weight-bold"> {inquiry.code}</span>
            </CardText>
            <CardSubtitle className="robotofont">
              Date of Booking:{" "}
              <span className="font-weight-bold">{inquiry.inquiryDate}</span>
            </CardSubtitle>
            <CardText className="robotofont">
              Status: <span className="font-weight-bold">{inquiry.status}</span>
            </CardText>
            <CardText className="robotofont">
              Booked Date:{" "}
              <span className="font-weight-bold">
                {inquiry.initialBookDate}
              </span>
            </CardText>
            <CardText className="robotofont">
              Booked Hours:{" "}
              <span className="font-weight-bold">
                {inquiry.initialBookHourFrom} - {inquiry.initialBookHourTo}
              </span>
            </CardText>
            <CardText className="robotofont">
              Number of Hours:{" "}
              <span className="font-weight-bold">{inquiry.totalHours}</span>
            </CardText>
            <CardText className="robotofont">
              Cost:{" "}
              <span className="font-weight-bold">{inquiry.totalPayment}</span>
            </CardText>
            <CardText>
              <small className="text-muted">
                {inquiry.status === "Paid" ? "Paid " : "Booked "}

                {moment(inquiry.logs.updateDate).fromNow()}
              </small>
            </CardText>
            <p></p>
            <Button
              color="success"
              className="robotofont"
              outline
              block
              onClick={handleShowStripe}
              disabled={inquiry.status === "Paid" ? true : false}
            >
              {inquiry.status === "Paid" ? "Paid For" : "Pay Now"}
            </Button>
            <Button
              color="danger"
              className="robotofont"
              outline
              block
              onClick={() => props.handleDeleteBooking(inquiry._id)}
            >
              Cancel
            </Button>
          </CardBody>
        </Card>
      </div>
    </React.Fragment>
  );
};

export default InquiryList;
