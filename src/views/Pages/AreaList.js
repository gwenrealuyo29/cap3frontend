import React, { useState, useEffect } from "react";
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button
} from "reactstrap";
import axios from "axios";
import BookForm from "./BookForm";
import moment from "moment";
import Swal from "sweetalert2";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const AreaList = props => {
  const [user, setUser] = useState([]);
  const [showForm, setShowForm] = useState(false);
  const [bookedDates, setBookedDates] = useState([]);
  const area = props.area;
  const [hourFrom, setHourFrom] = useState([]);
  const [pastHour, setPastHour] = useState([]);
  const [hourTo, setHourTo] = useState([]);
  const [selectedDay, setSelectedDay] = useState(undefined);
  const [population, setPopulation] = useState(1);
  const [onlyHour, setOnlyHour] = useState(1);
  const [meridian, setMeridian] = useState("");

  useEffect(() => {
    if (sessionStorage.token) {
      let user = JSON.parse(sessionStorage.user);
      setUser(user);
    } else {
      window.location.replace("#/login");
    }
  }, []);

  const handleShowForm = () => {
    setShowForm(!showForm);
    axios
      .get("https://nittygrittycoworking.herokuapp.com/admin/showinquiriesbyareaid/" + area._id)
      .then(res => {
        // console.log(res.data);
        setBookedDates(res.data.bookedDates);
        // console.log("booked dates", bookedDates);
      });
  };

  const handleDayClick = day => {
    setSelectedDay(day);
  };

  const handleFirstHour = value => {
    setHourFrom(moment(value).format("h:mm A"));
    console.log(hourFrom);
    
    // setOnlyHour(moment(value).format("h"));
    let onlyHour = moment(value).format("h");
    console.log(onlyHour);



    // setMeridian(moment(value).format("A"));
    let meridian = moment(value).format("A");
    console.log(meridian);


    if(onlyHour !=12 && meridian == "PM"){
        onlyHour = parseInt(onlyHour) + 12;
    }

    console.log(onlyHour);

    setPastHour([]);
    for(let i=0; i <= onlyHour; i++){
      pastHour.push(i);
    }

    setPastHour(pastHour);



  };

  const handleSecondHour = value => {
    setHourTo(moment(value).format("h:mm A"));
    console.log(hourTo);

    // let hour = hourFrom.split(":");
    // let ampm = hour[1].split(" ");

    // // console.log(hour, ampm);
    // // console.log(ampm[1]);

    // if (hour[0] != 12 && ampm[1] == "PM") {
    //   hour[0] = parseInt(hour[0]) + 12;
    // }

    // setPastHour([]);
    // // let closing = [0, 1, 2, 3, 4, 5];
    // for (let i = 0; i <= hour[0]; i++) {
    //   pastHour.push(i);
    // } 
    // setPastHour(pastHour);
    // // setPastHour(closing.concat(pastHour));
    // console.log(pastHour);
    // console.log(hourFrom);
  };

  const handlePopulationInput = e => {
    setPopulation(e.target.value);
  };

  const handleSaveBooking = () => {
    let firstHour = hourFrom.split(":");
    let firstMer = firstHour[1].split(" ");

    let secondHour = hourTo.split(":");
    let secondMer = secondHour[1].split(" ");
    // console.log(firstHour, firstMer);
    // console.log(secondHour, secondMer);
    let initHours;

    if (
      (firstHour[0] < secondHour[0] &&
        firstMer[1] == "AM" &&
        secondMer[1] == "PM") ||
      (secondHour[0] > firstHour[0] && secondMer[1] == "AM")
    ) {
      // console.log("first ", secondHour[0]);
      initHours = secondHour[0] - firstHour[0];
    } else if (
      firstHour[0] > secondHour[0] &&
      firstMer[1] == "AM" &&
      secondMer[1] == "PM" &&
      secondHour[0] < 12
    ) {
      // console.log("second ", secondHour[0]);
      initHours = 12 - firstHour[0];
      initHours = parseInt(initHours) + parseInt(secondHour[0]);
    } else if (
      firstHour[0] > secondHour[0] &&
      firstMer[1] == "AM" &&
      secondMer[1] == "PM"
    ) {
      // console.log("third ", secondHour[0]);
      initHours = secondHour[0] - firstHour[0];
    } else {
      // console.log("fourth ", secondHour[0]);
      // initHours = secondHour[0] - firstHour[0];
      initHours = secondHour[0];
    }

    // console.log(initHours);

    let initMins;
    if (secondMer[0] > firstMer[0]) {
      initMins = secondMer[0] - firstMer[0];
    } else if (secondMer[0] < firstMer[0]) {
      initMins = firstMer[0] - secondMer[0];
    } else if (secondMer[0] === firstMer[0]) {
      initMins = 0;
    }

    if (secondMer[0] === 30) {
      initHours = initHours - 1;
    }

    // console.log("initMins:", initMins);

    initMins == 30 ? (initMins = 5) : (initMins = 0);

    let totalHours = initHours + "." + initMins;
    parseInt(totalHours);

    // console.log("total: ", totalHours);

    let code = moment(new Date()).format("x");
    let totalPayment = initHours * props.area.price;

    let halfPay;
    if (initMins == 5) {
      halfPay = props.area.price / 2;
      totalPayment = totalPayment + halfPay;
    }

    // console.log(totalPayment);

    // console.log(selectedDay.toLocaleDateString());

    if (area.category == "Groupie" && population > 4 || area.category != "Groupie") {
      axios({
        method: "POST",
        url: "https://nittygrittycoworking.herokuapp.com/admin/addinquiry",
        data: {
          code: code,
          population: population,
          inquirer: user.email,
          inquirerId: user.id,
          areaName: area.name,
          areaId: area._id,
          initialBookDate: selectedDay.toLocaleDateString(),
          initialBookHourFrom: hourFrom,
          initialBookHourTo: hourTo,
          totalPayment: totalPayment,
          totalHours: totalHours
        }
      }).then(res => {
        console.log(res.data);
        setShowForm(false);
      });

      Swal.fire({
        icon: "success",
        title: "Your booking has been saved!",
        showConfirmButton: false,
        timer: 1500
      });

      window.location.replace("#/mybookings");
      setTimeout(function () {
          window.location.reload()
      }, 1000);
    } else {
      toast.error("Number of People should be 5 or more", {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
    }
  };

  return (
    <React.Fragment>
      <ToastContainer
        position="top-center"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnVisibilityChange
        draggable
        pauseOnHover
      />
      <BookForm
        showForm={showForm}
        handleShowForm={handleShowForm}
        area={area}
        bookedDates={bookedDates}
        handleDayClick={handleDayClick}
        selectedDay={selectedDay}
        handleSaveBooking={handleSaveBooking}
        handleFirstHour={handleFirstHour}
        handleSecondHour={handleSecondHour}
        handlePopulationInput={handlePopulationInput}
        population={population}
        hourFrom={hourFrom}
        hourTo={hourTo}
        pastHour={pastHour}
      />
      <div className="col-lg-4 my-5">
        <Card body className="text-center">
          <CardImg
            top
            width="100%"
            src={
              area.category == "Groupie"
                ? "https://images.pexels.com/photos/1170412/pexels-photo-1170412.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
                : area.category == "Soloist"
                ? "https://images.pexels.com/photos/159839/office-home-house-desk-159839.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
                : "https://images.pexels.com/photos/37347/office-sitting-room-executive-sitting.jpg?auto=compress&cs=tinysrgb&dpr=1&w=500"
            }
            alt="IMAGE COMING SOON!!!"
          />
          <CardBody>
            <CardTitle className="chonburifont">{area.name}</CardTitle>
            <CardSubtitle className="robotofont">
              {area.description}
            </CardSubtitle>
            <CardText className="robotofont">{area.status}</CardText>
            <CardText className="robotofont">
              PHP {area.price}.00 per hour
            </CardText>
            <Button
              outline
              color="warning"
              className="robotofont"
              onClick={handleShowForm}
            >
              BOOK
            </Button>
          </CardBody>
        </Card>
      </div>
    </React.Fragment>
  );
};

export default AreaList;
