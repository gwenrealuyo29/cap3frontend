import React, { useState, useEffect } from "react";
import { Modal, ModalHeader, ModalBody, Button } from "reactstrap";
import { Elements, StripeProvider } from "react-stripe-elements";
import CheckoutForm from "./CheckoutForm";

const Payment = props => {
  const [user, setUser] = useState([]);
  const baseurl = "https://nittygrittycoworking.herokuapp.com";
  useEffect(() => {
    if (sessionStorage.token) {
      let user = JSON.parse(sessionStorage.user);
      // alert(user);
      setUser(user);
    } else {
      window.location.replace("#/login");
    }
  }, []);

  return (
    <div>
      <Modal isOpen={props.showStripe} toggle={props.handleShowStripe} centered>
        <ModalHeader toggle={props.handleShowStripe}>
          Booking Area Name:{" "}
          <span className="chonburifont">{props.inquiry.areaName}</span>
        </ModalHeader>
        <ModalBody>
          <p className="robotofont">
            Inquired:{" "}
            <span className="font-weight-bold">
              {props.inquiry.inquiryDate}
            </span>{" "}
          </p>
          <p className="robotofont">
            Hours Booked:{" "}
            <span className="font-weight-bold">{props.inquiry.totalHours}</span>
          </p>
          <p className="robotofont">
            Cost:{" "}
            <span className="font-weight-bold">
              PHP {props.inquiry.totalPayment}.00
            </span>
          </p>

          <StripeProvider apiKey="pk_test_EZKkSHj63HQeVvsW62Qr7FTp00bYQ5PJB0">
            <div>
              <div className="d-flex justify-content-center">
                <Elements>
                  <CheckoutForm
                    inquiry={props.inquiry}
                    user={props.user}
                    showStripe={props.showStripe}
                    handleShowStripe={props.handleShowStripe}
                    handleCancelBooking={props.handleCancelBooking}
                  />
                </Elements>
              </div>
            </div>
          </StripeProvider>
        </ModalBody>
      </Modal>
    </div>
  );
};

export default Payment;
