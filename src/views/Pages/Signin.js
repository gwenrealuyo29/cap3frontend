import React, { Component } from "react";
import "../../App.scss";
import { Login, Register } from "../../globalcomponents/login/index";
import axios from "axios";
import Swal from "sweetalert2";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

class Signin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogginActive: true,
      users: "",
      username: "",
      email: "",
      password: ""
    };
  }

  handleUsernameChange = e => {
    this.setState({ username: e.target.value });
    console.log(this.state.username.length);
  };

  handleEmailChange = e => {
    this.setState({ email: e.target.value });
    // console.log(this.state.email);
  };

  handlePasswordChange = e => {
    this.setState({ password: e.target.value });
    // console.log(this.state.password);
  };

  handleRegister = () => {
    if (this.state.username.length >= 8) {
      axios
        .post("https://nittygrittycoworking.herokuapp.com/register", {
          username: this.state.username,
          email: this.state.email,
          password: this.state.password
        })
        .then(res => {
          console.log(res.data);
        });

      Swal.fire({
        icon: "success",
        title: "You are now registered!",
        width: 600,
        padding: "3em",
        backdrop: `
          rgba(0,0,123,0.4)
          left top
          no-repeat
        `
      });

      this.changeState();
    } else {
      toast.error("Username must contain at least 8 characters", {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      });
    }
  };

  handleLogin = () => {
    axios
      .post("https://nittygrittycoworking.herokuapp.com/login", {
        email: this.state.email,
        password: this.state.password
      })
      .then(res => {
        sessionStorage.token = res.data.token;
        sessionStorage.user = JSON.stringify(res.data.user);
        console.log(sessionStorage.token);
        window.location.replace("#/home");
      });

    // toast.error("Please enter valid email or password", {
    //   position: "top-center",
    //   autoClose: 5000,
    //   hideProgressBar: false,
    //   closeOnClick: true,
    //   pauseOnHover: true,
    //   draggable: true
    // });
  };

  componentDidMount() {
    //Add .right by default
    this.rightSide.classList.add("right");
  }

  changeState() {
    const { isLogginActive } = this.state;

    if (isLogginActive) {
      this.rightSide.classList.remove("right");
      this.rightSide.classList.add("left");
    } else {
      this.rightSide.classList.remove("left");
      this.rightSide.classList.add("right");
    }
    this.setState(prevState => ({ isLogginActive: !prevState.isLogginActive }));
  }

  render() {
    const { isLogginActive } = this.state;
    const current = isLogginActive ? "Register" : "Login";
    const currentActive = isLogginActive ? "login" : "register";

    return (
      <div className="App">
        <ToastContainer
          position="top-center"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnVisibilityChange
          draggable
          pauseOnHover
        />
        <div className="login">
          <div className="container" ref={ref => (this.container = ref)}>
            {isLogginActive && (
              <Login
                containerRef={ref => (this.current = ref)}
                handleEmailChange={this.handleEmailChange}
                email={this.state.email}
                handlePasswordChange={this.handlePasswordChange}
                password={this.state.password}
                handleLogin={this.handleLogin}
              />
            )}
            {!isLogginActive && (
              <Register
                containerRef={ref => (this.current = ref)}
                handleUsernameChange={this.handleUsernameChange}
                username={this.state.username}
                handleEmailChange={this.handleEmailChange}
                email={this.state.email}
                handlePasswordChange={this.handlePasswordChange}
                password={this.state.password}
                handleRegister={this.handleRegister}
              />
            )}
          </div>
          <RightSide
            current={current}
            currentActive={currentActive}
            containerRef={ref => (this.rightSide = ref)}
            onClick={this.changeState.bind(this)}
          />
        </div>
      </div>
    );
  }
}

const RightSide = props => {
  return (
    <div
      className="right-side"
      ref={props.containerRef}
      onClick={props.onClick}
    >
      <div className="inner-container">
        <div className="text">{props.current}</div>
      </div>
    </div>
  );
};

export default Signin;
