import React, { useState, useEffect } from "react";
import { CardElement, injectStripe } from "react-stripe-elements";
import axios from "axios";
import { Button } from "reactstrap";
import moment from "moment";
import Swal from "sweetalert2";

const CheckoutForm = props => {
  const inquiry = props.inquiry;
  const [user, setUser] = useState([]);
  const [inquiries, setInquiries] = useState([]);
  const [stripeRequired, setStripeRequired] = useState(false);

  useEffect(() => {
    if (sessionStorage.token) {
      let user = JSON.parse(sessionStorage.user);
      setUser(user);
      fetch("https://nittygrittycoworking.herokuapp.com/admin/showinquiriesbyuser/" + user.id)
        .then(res => res.json())
        .then(data => setInquiries(data));
    } else {
      window.location.replace("#/login");
    }
  }, []);

  const handlePayAndBookClass = async e => {
    if (sessionStorage.token) {
      let { token } = await props.stripe.createToken({ name: "Name" });
      let user = JSON.parse(sessionStorage.user);
      console.log(user);
      setUser(user);

      axios
        .post("https://nittygrittycoworking.herokuapp.com/charge", {
          amount: inquiry.totalPayment * 100,
          source: token.id,
          email: user.email
        })
        .then(
          Swal.fire({
            icon: "success",
            title: "Your booking is paid",
            showConfirmButton: false,
            timer: 1500
          })
        );

      let log = {
        status: "Paid",
        updateDate: moment(new Date()).format("MM-DD-YYYY"),
        message: "Paid by Customer"
      };

      axios
        .patch(
          "https://nittygrittycoworking.herokuapp.com/admin/updateinquirystatus/" + inquiry._id,
          {
            status: "Paid",
            logs: log
          }
        )
        .then(res => {
          let index = inquiries.findIndex(i => i._id === inquiry._id);
          let newInquiry = [...inquiries];
          newInquiry.splice(index, 1, res.data);
          setInquiries(newInquiry);
        });

      setInquiries(inquiries);


      props.handleCancelBooking();
      // setTimeout(window.location.reload(true), 80000);
      setTimeout(function () {
          window.location.reload()
      }, 3000);
    }
  };

  const handleonChange = (e) => {
    if(e.complete === false){
      setStripeRequired(false)
    }else {
      setStripeRequired(true)
    }

  }


  return (
    <div className="checkout">
      <p className="robotofont">
        Would you like to complete the reservation for{" "}
        <span className="chonburifont">{props.inquiry.areaName}</span>?
      </p>
      <p className="robotofont">Enter card information below to proceed: </p>
      <CardElement onChange={handleonChange} />
      <div className="d-flex justify-content-center mt-3">
        <Button
          outline
          color="success"
          className="mx-1 robotofont"
          onClick={handlePayAndBookClass}
          disabled={stripeRequired === false ? true : false}
        >
          Book Now
        </Button>
        <Button
          outline
          color="danger"
          className="mx-1 robotofont"
          onClick={props.handleCancelBooking}
        >
          Cancel
        </Button>
      </div>
    </div>
  );
};

export default injectStripe(CheckoutForm);
