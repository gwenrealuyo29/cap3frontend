import React, { useEffect, useState } from "react";
import { AreasRow, AreasForm } from "./components";
import axios from "axios";
import Swal from "sweetalert2";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Button } from "reactstrap";
import { NavBar } from "../Layout";

const Areas = () => {
  const baseurl = "https://nittygrittycoworking.herokuapp.com";

  const [areas, setAreas] = useState([]);
  const [showForm, setShowForm] = useState(false);
  const [code, setCode] = useState("");
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [category, setCategory] = useState("");
  const [availability, setAvailability] = useState(1);
  const [status, setStatus] = useState("");
  const [price, setPrice] = useState(0);
  const [editId, setEditId] = useState("");
  const [showStatus, setShowStatus] = useState(false);
  const [showAvail, setShowAvail] = useState(false);
  const [showName, setShowName] = useState(false);
  const [showCategory, setShowCategory] = useState(false);
  const [showDescription, setShowDescription] = useState(false);
  const [showPrice, setShowPrice] = useState(false);
  const [showCode, setShowCode] = useState(false);
  const [user, setUser] = useState([]);

  useEffect(() => {
    if (sessionStorage.token) {
      let user = JSON.parse(sessionStorage.user);
      setUser(user);
      if(user.isAdmin){
          fetch(baseurl + "/admin/showareas")
          .then(res => res.json())
          .then(data => setAreas(data));
      } else {
        window.location.replace('#/home')
      }

      
    } else {
      window.location.replace("#/login");
    }
    
  }, []);

  const handleShowForm = () => {
    setShowForm(!showForm);
  };

  const handleRefresh = () => {
    setShowForm(false);
    setCode("");
    setName("");
    setDescription("");
    setCategory("");
    setAvailability(1);
    setPrice(0);
    setEditId("");
  };

  const handleDeleteArea = area => {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!"
    }).then(result => {
      if (result.value) {
        Swal.fire("Deleted!", "It has been deleted.", "success");

        axios.delete(baseurl + "/admin/deletearea/" + area).then(res => {
          let index = areas.findIndex(area => area._id === area);
          let newAreas = [...areas];
          newAreas.splice(index, 1);
          setAreas(newAreas);
        });
      }
    });
  };

  const handleAreaCodeChange = e => {
    if (e.target.value !== "") {
      setCode(e.target.value);
    } else {
      setCode(e.target.value);
    }
  };

  const handleAreaNameChange = e => {
    if (e.target.value !== "") {
      setName(e.target.value);
    } else {
      setName(e.target.value);
    }
  };

  const handleAreaDescriptionChange = e => {
    if (e.target.value !== "") {
      setDescription(e.target.value);
    } else {
      setDescription(e.target.value);
    }
  };

  const handleAreaCategoryChange = e => {
    if (e.target.value !== "") {
      setCategory(e.target.value);
    } else {
      setCategory(e.target.value);
    }
  };

  const handleAreaAvailabilityChange = e => {
    if (e.target.value !== "") {
      setAvailability(e.target.value);
    } else {
      setAvailability(e.target.value);
    }
  };

  const handleAreaPriceChange = e => {
    if (e.target.value !== "") {
      setPrice(e.target.value);
    } else {
      setPrice(e.target.value);
    }
  };

  const handleSaveArea = () => {
    axios
      .post(baseurl + "/admin/addarea", {
        code,
        name,
        description,
        category,
        availability,
        price
      })
      .then(res => {
        let newAreas = [...areas];
        newAreas.push(res.data);
        setAreas(newAreas);
        handleRefresh();
      });

    Swal.fire({
      icon: "success",
      title: "New area has been added!",
      showConfirmButton: false,
      timer: 1500
    });
  };

  const invalidInput = () => {
    toast.error("Invalid Input. Try again please!", {
      position: "bottom-center",
      autoClose: 3000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true
    });
  };

  const handleStatusInput = editId => {
    setEditId(editId);
    setShowStatus(true);
    setShowAvail(false);
    setShowName(false);
    setShowCategory(false);
    setShowDescription(false);
    setShowPrice(false);
    setShowCode(false);
  };

  const handleAvailInput = editId => {
    setEditId(editId);
    setShowAvail(true);
    setShowStatus(false);
    setShowName(false);
    setShowCategory(false);
    setShowDescription(false);
    setShowPrice(false);
    setShowCode(false);
  };

  const handleNameInput = editId => {
    setEditId(editId);
    setShowName(true);
    setShowAvail(false);
    setShowStatus(false);
    setShowCategory(false);
    setShowDescription(false);
    setShowPrice(false);
    setShowCode(false);
  };

  const handleCategoryInput = editId => {
    setEditId(editId);
    setShowName(false);
    setShowAvail(false);
    setShowStatus(false);
    setShowCategory(true);
    setShowDescription(false);
    setShowPrice(false);
    setShowCode(false);
  };

  const handleDescriptionInput = editId => {
    setEditId(editId);
    setShowName(false);
    setShowAvail(false);
    setShowStatus(false);
    setShowCategory(false);
    setShowDescription(true);
    setShowPrice(false);
    setShowCode(false);
  };

  const handlePriceInput = editId => {
    setEditId(editId);
    setShowName(false);
    setShowAvail(false);
    setShowStatus(false);
    setShowCategory(false);
    setShowDescription(false);
    setShowPrice(true);
    setShowCode(false);
  };

  const handleCodeInput = editId => {
    setEditId(editId);
    setShowName(false);
    setShowAvail(false);
    setShowStatus(false);
    setShowCategory(false);
    setShowDescription(false);
    setShowPrice(false);
    setShowCode(true);
  };

  const handleEditStatus = status => {
    axios
      .patch(baseurl + "/admin/updatestatus/" + editId, {
        status
      })
      .then(res => {
        let index = areas.findIndex(a => a._id === editId);
        let newAreas = [...areas];
        newAreas.splice(index, 1, res.data);
        setAreas(newAreas);
        setEditId("");
      });
  };

  const handleEditCategory = category => {
    if (category === "") {
      setEditId("");
    } else {
      axios
        .patch(baseurl + "/admin/updatecategory/" + editId, {
          category
        })
        .then(res => {
          let index = areas.findIndex(a => a._id === editId);
          let newAreas = [...areas];
          newAreas.splice(index, 1, res.data);
          setAreas(newAreas);
          setEditId("");
        });
    }
  };

  const handleEditAvail = (e, editId) => {
    let availability = e.target.value;

    if (availability <= 0) {
      setEditId("");
      invalidInput();
    } else {
      axios
        .patch(baseurl + "/admin/updateavailability/" + editId, {
          availability
        })
        .then(res => {
          let index = areas.findIndex(a => a._id === editId);
          let newAreas = [...areas];
          newAreas.splice(index, 1, res.data);
          setAreas(newAreas);
          setEditId("");
        });
    }
  };

  const handleEditName = (e, editId) => {
    let name = e.target.value;

    if (name === "") {
      setEditId("");
      invalidInput();
    } else {
      axios
        .patch(baseurl + "/admin/updatename/" + editId, {
          name
        })
        .then(res => {
          let index = areas.findIndex(a => a._id === editId);
          let newAreas = [...areas];
          newAreas.splice(index, 1, res.data);
          setAreas(newAreas);
          setEditId("");
        });
    }
  };

  const handleEditDescription = (e, editId) => {
    let description = e.target.value;

    if (description === "") {
      setEditId("");
      invalidInput();
    } else {
      axios
        .patch(baseurl + "/admin/updatedescription/" + editId, {
          description
        })
        .then(res => {
          let index = areas.findIndex(a => a._id === editId);
          let newAreas = [...areas];
          newAreas.splice(index, 1, res.data);
          setAreas(newAreas);
          setEditId("");
        });
    }
  };

  const handleEditCode = (e, editId) => {
    let code = e.target.value;

    if (code === "") {
      setEditId("");
      invalidInput();
    } else {
      axios
        .patch(baseurl + "/admin/updatecode/" + editId, {
          code
        })
        .then(res => {
          let index = areas.findIndex(a => a._id === editId);
          let newAreas = [...areas];
          newAreas.splice(index, 1, res.data);
          setAreas(newAreas);
          setEditId("");
        });
    }
  };

  const handleEditPrice = (e, editId) => {
    let price = e.target.value;

    if (price === "" || price <= 0) {
      setEditId("");
      invalidInput();
    } else {
      axios
        .patch(baseurl + "/admin/updateprice/" + editId, {
          price
        })
        .then(res => {
          let index = areas.findIndex(a => a._id === editId);
          let newAreas = [...areas];
          newAreas.splice(index, 1, res.data);
          setAreas(newAreas);
          setEditId("");
        });
    }
  };

  return (
    <React.Fragment>
      <NavBar />
      <ToastContainer />
      <div className="col-lg-10 offset-lg-1 pt-5">
        <h1 className="text-center mulifont pt-5 px-5">Workspace Areas</h1>

        <div className="d-flex justify-content-end robotofont">
          <Button outline color="warning" onClick={handleShowForm}>
            <i className="fas fa-plus"></i> Area
          </Button>
          <AreasForm
            showForm={showForm}
            areas={areas}
            name={name}
            code={code}
            description={description}
            availability={availability}
            category={category}
            price={price}
            handleShowForm={handleShowForm}
            handleAreaCodeChange={handleAreaCodeChange}
            handleAreaNameChange={handleAreaNameChange}
            handleAreaDescriptionChange={handleAreaDescriptionChange}
            handleAreaCategoryChange={handleAreaCategoryChange}
            handleAreaAvailabilityChange={handleAreaAvailabilityChange}
            handleAreaPriceChange={handleAreaPriceChange}
            handleSaveArea={handleSaveArea}
          />
        </div>

        <table className="table table-striped table-hover border mt-2 robotofont">
          <thead>
            <tr>
              <th>Code</th>
              <th>Name</th>
              <th>Description</th>
              <th>Category</th>
              <th>Quantity</th>
              <th>Status</th>
              <th>Price</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {areas.map(area => (
              <AreasRow
                key={area._id}
                area={area}
                areas={areas}
                handleDeleteArea={handleDeleteArea}
                editId={editId}
                status={status}
                handleStatusInput={handleStatusInput}
                showStatus={showStatus}
                handleEditStatus={handleEditStatus}
                handleAvailInput={handleAvailInput}
                showAvail={showAvail}
                handleEditAvail={handleEditAvail}
                showName={showName}
                handleNameInput={handleNameInput}
                handleEditName={handleEditName}
                showCategory={showCategory}
                handleCategoryInput={handleCategoryInput}
                handleEditCategory={handleEditCategory}
                showDescription={showDescription}
                handleDescriptionInput={handleDescriptionInput}
                handleEditDescription={handleEditDescription}
                showPrice={showPrice}
                handlePriceInput={handlePriceInput}
                handleEditPrice={handleEditPrice}
                showCode={showCode}
                handleCodeInput={handleCodeInput}
                handleEditCode={handleEditCode}
              />
            ))}
          </tbody>
        </table>
      </div>
    </React.Fragment>
  );
};

export default Areas;
