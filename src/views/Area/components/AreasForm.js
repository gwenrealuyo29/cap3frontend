import React from "react";
import { Modal, ModalBody, ModalHeader, Button } from "reactstrap";
import { FormInput } from "../../../globalcomponents";

const AreasForm = props => {
  return (
    <Modal isOpen={props.showForm} toggle={props.handleShowForm}>
      <ModalHeader toggle={props.handleShowForm}>Add an Area</ModalHeader>
      <ModalBody>
        <FormInput
          label={"Code"}
          type={"text"}
          name={"code"}
          placeholder={"Enter code"}
          autoFocus={true}
          onChange={props.handleAreaCodeChange}
        />
        <FormInput
          label={"Area Name"}
          type={"text"}
          name={"name"}
          placeholder={"Enter name"}
          onChange={props.handleAreaNameChange}
        />
        <FormInput
          label={"Description"}
          type={"text"}
          name={"description"}
          placeholder={"Enter description"}
          onChange={props.handleAreaDescriptionChange}
        />
        <FormInput
          label={"Category"}
          type={"text"}
          name={"category"}
          placeholder={"Enter category"}
          onChange={props.handleAreaCategoryChange}
        />
        <FormInput
          label={"Available Areas"}
          type={"number"}
          name={"availability"}
          placeholder={"Enter quantity of available areas"}
          defaultValue={1}
          onChange={props.handleAreaAvailabilityChange}
        />
        <FormInput
          label={"Price"}
          type={"number"}
          name={"price"}
          placeholder={"Enter price"}
          onChange={props.handleAreaPriceChange}
        />
        <Button
          color="warning"
          block
          outline
          onClick={props.handleSaveArea}
          disabled={
            props.code === "" ||
            props.name === "" ||
            props.description === "" ||
            props.category === "" ||
            props.availability === "" ||
            props.availability <= 0 ||
            props.price === "" ||
            props.price <= 0
              ? true
              : false
          }
        >
          Add
        </Button>
      </ModalBody>
    </Modal>
  );
};

export default AreasForm;
