import React, { useState } from "react";
import {
  Button,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";
import { FormInput } from "../../../globalcomponents";

const AreasRow = props => {
  const [dropdownOpen, setDropdownOpen] = useState(false);

  const toggle = () => {
    setDropdownOpen(!dropdownOpen);
  };
  const area = props.area;
  return (
    <React.Fragment>
      <tr>
        <td onClick={() => props.handleCodeInput(area._id)}>
          {props.showCode && props.editId === area._id ? (
            <FormInput
              defaultValue={area.code}
              autoFocus={true}
              type={"text"}
              onBlur={e => props.handleEditCode(e, area._id)}
            />
          ) : (
            area.code
          )}
        </td>
        <td onClick={() => props.handleNameInput(area._id)}>
          {props.showName && props.editId === area._id ? (
            <FormInput
              defaultValue={area.name}
              autoFocus={true}
              type={"text"}
              onBlur={e => props.handleEditName(e, area._id)}
            />
          ) : (
            area.name
          )}
        </td>
        <td onClick={() => props.handleDescriptionInput(area._id)}>
          {props.showDescription && props.editId === area._id ? (
            <FormInput
              defaultValue={area.description}
              autoFocus={true}
              type={"text"}
              onBlur={e => props.handleEditDescription(e, area._id)}
            />
          ) : (
            area.description
          )}
        </td>
        <td onClick={() => props.handleCategoryInput(area._id)}>
          {props.showCategory && props.editId === area._id ? (
            <Dropdown isOpen={dropdownOpen} toggle={toggle}>
              <DropdownToggle caret>{area.category}</DropdownToggle>
              <DropdownMenu>
                <DropdownItem
                  onClick={() => props.handleEditCategory("Soloist")}
                >
                  Soloist
                </DropdownItem>
                <DropdownItem
                  onClick={() => props.handleEditCategory("Partners in Crime")}
                >
                  Partners in Crime
                </DropdownItem>
                <DropdownItem
                  onClick={() => props.handleEditCategory("Groupie")}
                >
                  Groupie
                </DropdownItem>
                <DropdownItem
                  onClick={() =>
                    props.handleEditCategory("Party (more than 10)")
                  }
                >
                  Party (more than 10)
                </DropdownItem>
              </DropdownMenu>
            </Dropdown>
          ) : (
            area.category
          )}
        </td>
        <td onClick={() => props.handleAvailInput(area._id)}>
          {props.showAvail && props.editId === area._id ? (
            <FormInput
              defaultValue={area.availability}
              autoFocus={true}
              type={"number"}
              onBlur={e => props.handleEditAvail(e, area._id)}
            />
          ) : (
            area.availability
          )}
        </td>
        <td onClick={() => props.handleStatusInput(area._id)}>
          {props.showStatus && props.editId === area._id ? (
            <Dropdown isOpen={dropdownOpen} toggle={toggle}>
              <DropdownToggle caret>{area.status}</DropdownToggle>
              <DropdownMenu>
                <DropdownItem onClick={() => props.handleEditStatus("Vacant")}>
                  Vacant
                </DropdownItem>
                <DropdownItem
                  onClick={() => props.handleEditStatus("All Occupied")}
                >
                  All Occupied
                </DropdownItem>
                <DropdownItem
                  onClick={() => props.handleEditStatus("For Maintenance")}
                >
                  For Maintenance
                </DropdownItem>
              </DropdownMenu>
            </Dropdown>
          ) : (
            area.status
          )}
        </td>
        <td onClick={() => props.handlePriceInput(area._id)}>
          {props.showPrice && props.editId === area._id ? (
            <FormInput
              defaultValue={area.price}
              autoFocus={true}
              type={"number"}
              onBlur={e => props.handleEditPrice(e, area._id)}
            />
          ) : area.price <= 0 || area.price === undefined ? (
            "Php 0.00"
          ) : (
            "Php " + area.price + ".00"
          )}
        </td>
        <td>
          <Button
            outline
            color="danger"
            onClick={() => props.handleDeleteArea(area._id)}
          >
            <i className="fas fa-trash-alt"></i>
          </Button>
        </td>
      </tr>
    </React.Fragment>
  );
};

export default AreasRow;
